<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZaduzenjasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zaduzenjas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clan_id');
            $table->integer('knjiga_id');
            $table->integer('status');
            $table->string('datum_zaduzenja');
            $table->string('datum_vracanja');
            $table->string('rok_vracanja');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zaduzenjas');
    }
}
