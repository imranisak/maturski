@extends('master-admin')
@section('head')
<title> Uredi {{$data->ime_prezime}} </title>
@endsection

@section('content')
<form action="/admin/pisci/uredi/{{$data->id}}" method="post">
	{{csrf_field()}}
	<div class="form-group col-md-3">
  		<label class="col-form-label" for="inputDefault">Ime i prezime</label>
  		<input type="text" class="form-control" placeholder="Ime i prezime" id="inputDefault" name="ime_prezime" value=" {{$data->ime_prezime}} " required>  		
  		<label class="col-form-label" for="inputDefault" >Nacionalnost</label>
  		<input type="text" class="form-control" placeholder="Nacionalnost" id="inputDefault" name="nacionalnost" required value=" {{$data->nacionalnost}} " >
  		<label class="col-form-label" for="inputDefault">Godina rođenja</label>
  		<input type="number" class="form-control" placeholder="Rođenje" id="inputDefault" name="rodjenje" required value="{{$data->rodjenje}}">  		
  		<label class="col-form-label" for="inputDefault">Godina smrti</label>
  		<input type="number" class="form-control" placeholder="Smrt" id="inputDefault" name="smrt" value="{{$data->smrt}}">  		
	</div>
	<input type="submit" value="Uredi" class="btn btn-primary">
</form>
@endsection