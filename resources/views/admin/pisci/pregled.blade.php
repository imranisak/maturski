@extends('master-admin')
@section('head')
<title>Pregled svih pisaca</title>

@endsection

@section('content')
@include('partials.pik_admin')
<table class="table table-hover">
  <thead>
    <tr class="table-light">
      <b>
      <td scope="row">Ime i prezime</td>
      </b>
      <td>Godina rođenja i smrti</td>
      <td>Nacionalnost</td>
      <td>Opcije</td>
  	  </b>
    </tr>
   </thead>
   <tbody>
   		@foreach($data as $p)
   		<tr>
   			<td><a href="/admin/pisci/detalji/{{$p->id}}"> {{$p->ime_prezime}} </a> </td>
   			<td> {{$p->rodjenje}} @if($p->smrt!=0) - {{$p->smrt}} @endif  </td>
        <td> {{$p->nacionalnost}} </td>
   			<td> 
          <a href="/admin/pisci/uredi/{{$p->id}}" class="material-icons" style="font-size: 20px;color:black"> edit </a> 
          <a href="/admin/pisci/del/{{$p->id}}" class="material-icons" style="font-size: 20px;color:red"> delete_forever</a>
        </td>
   		</tr>
   		@endforeach
   </tbody>
</table> 


@endsection