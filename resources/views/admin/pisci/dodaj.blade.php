@extends('master-admin')
@section('head')
<title>Dodaj pisca</title>
@endsection

@section('content')
<h3>Dodaj pisca</h3>
<form action="/admin/pisci/dodaj" method="post">
	{{csrf_field()}}
	<div class="form-group col-md-3">
  		<label class="col-form-label" for="inputDefault">Ime i prezime</label>
  		<input type="text" class="form-control" placeholder="Ime i prezime" id="inputDefault" name="ime_prezime" required>  		
  		<label class="col-form-label" for="inputDefault">Nacionalnost</label>
  		<input type="text" class="form-control" placeholder="Nacionalnost" id="inputDefault" name="nacionalnost" required>
  		<label class="col-form-label" for="inputDefault">Godina rođenja</label>
  		<input type="number" class="form-control" placeholder="Rođenje" id="inputDefault" name="rodjenje" required>  		
  		<label class="col-form-label" for="inputDefault">Godina smrti</label>
  		<input type="number" class="form-control" placeholder="Smrt" id="inputDefault" name="smrt">  		
	</div>
	<input type="submit" value="Snimi" class="btn btn-primary">
</form>


@endsection