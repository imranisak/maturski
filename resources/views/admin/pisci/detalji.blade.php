@extends('master-admin')
@section('head')
<title> {{$data->ime_prezime}} </title>
@endsection

@section('content')
<h3>Ime i prezime: {{$data->ime_prezime}} </h3>
<h3>Nacionalnost: {{$data->nacionalnost}} </h3>
<h3>Godina rođenja: {{$data->rodjenje}} </h3>
@if($data->smrt != 0)
<h3>Godina smrti: {{$data->smrt}} </h3>
@endif

<h4>Knjige od {{$data->ime_prezime}}</h4>

@foreach($knjige as $k)

<a href="/admin/knjiga/detalji/{{$k->id}}"> {{$k->naslov}} </a> <br>

@endforeach


@endsection