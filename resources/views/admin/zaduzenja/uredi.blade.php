@extends('master-admin')
@section('head')
<title>Uredi zaduženje</title>
@endsection

@section('content')

<h3>Uredi zaduženje</h3>
<form action="/admin/zaduzenja/uredi/{{$data->id}}" method="post">
	{{csrf_field()}}
	<div class="form-group col-md-3">
  		<label class="col-form-label" for="inputDefault">Koji član</label>
  		<select class="custom-select" required name="clan_id">
			<option selected disabled>Odaberi člana</option>
			@foreach($clan as $c)
			@if($c->status==1)
			<option value="{{$c->id}}" @if($c->id==$data->clan_id) selected @endif  >{{$c->ime_prezime}}</option>
			@endif
			@endforeach
		</select>


  		<label class="col-form-label" for="inputDefault">Odaberi knjigu</label>
		<select class="custom-select" required name="knjiga_id">
			<option selected disabled>Odaberi knjigu</option>
			@foreach($knjige as $k)
			<option value="{{$k->id}}" @if($k->id==$data->knjiga_id) selected @endif @if($k->količina==0) disabled @endif>{{$k->naslov}} ({{$k->količina}}) @if($k->količina==0) (Nije na stanju) @endif </option>
			@endforeach
		</select>		

  		<label class="col-form-label" for="inputDefault">Datum zaduženja</label>
		<input type="date" name="datum_zaduzenja" class="form-control"  value="{{$data->datum_zaduzenja}}" required>

  		<label class="col-form-label" for="inputDefault">Rok vraćanja</label>
		<input type="date" name="rok_vracanja" class="form-control"   value="{{$data->rok_vracanja}}" required>
		@if($data->datum_vracanja!=0)	

		<label class="col-form-label" for="inputDefault">Datum vraćanja</label>
		<input type="date" name="datum_vracanja" class="form-control"   value="{{$data->datum_vracanja}}" required>
		@endif


	</div>
	<input type="submit" value="Snimi" class="btn btn-primary">
</form>


@endsection