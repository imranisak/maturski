@extends('master-admin')
@section('head')
<title>Dodaj zaduženje</title>
@endsection

@section('content')

<h3>Dodaj zaduženje</h3>
<form action="/admin/zaduzenja/dodaj" method="post">
	{{csrf_field()}}
	<div class="form-group col-md-3">
  		<label class="col-form-label" for="inputDefault">Koji član</label>
  		<select class="custom-select" required name="clan_id">
			<option selected disabled>Odaberi člana</option>
			@foreach($clan as $c)
			@if($c->status==1)
			<option value="{{$c->id}}">{{$c->ime_prezime}}</option>
			@endif
			@endforeach
		</select>


  		<label class="col-form-label" for="inputDefault" required >Odaberi knjigu</label>
		<select class="custom-select" required name="knjiga_id">
			<option selected disabled value="0">Odaberi knjigu</option>
			@foreach($knjige as $k)
			<option value="{{$k->id}}" @if($k->količina==0) disabled @endif>{{$k->naslov}} ({{$k->količina}}) @if($k->količina==0) (Nije na stanju) @endif </option>
			@endforeach
		</select>		

  		<label class="col-form-label" for="inputDefault">Datum zaduženja</label>
		<input type="date" name="datum_zaduzenja" class="form-control"  value="@php echo date('Y-m-d'); @endphp" required>

  		<label class="col-form-label" for="inputDefault">Rok vraćanja</label>
		<input type="date" name="rok_vracanja" class="form-control"   value="@php echo date('Y-m-d', strtotime(' + 14 days')); @endphp" required>
		<input type="hidden" name="status" value="1">
	</div>
	<input type="submit" value="Snimi" class="btn btn-primary">
</form>


@endsection