@extends('master-admin')
@section('head')
<title>Detalji za {{$data->naslov}}  </title>
@endsection

@section('content')
<h1>Naslov:<span>  {{$data->naslov}} </span></h1> 
<h3>Pisac:<span> <a href="/admin/pisci/detalji/{{$data->pisac->id}}" target="_blank"> {{$data->pisac->ime_prezime}} </a></span></h3> 
<h3>Vrsta djela:<span> <a href="/admin/vrsta/detalji/{{$data->vrsta->id}}" target="_blank"> {{$data->vrsta->vrsta}} </a></span></h3> 
<h3>Količina na stanju:<span> {{$data->količina}} </a></span></h3> 
<h3>Lokacija:<span> {{$data->lokacija->ime}} ({{$data->lokacija->adresa}}) </span></h3> 

<a href="/admin/knjiga/uredi/{{$data->id}}" class="btn btn-primary">Uredi</a>

@endsection