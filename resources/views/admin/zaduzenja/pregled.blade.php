@extends('master-admin')
@section('head')
<title>Pregled zaduženja</title>
@endsection

@section('content')

<table class="table table-hover">
  <thead>
    <tr class="table-light">
      <td scope="row">Član</td>
      <b>
      <td>Knjiga</td>
      <td>Status</td>
      <td>Datum zaduženja</td>
      <td>Rok vraćanja</td>
      <td>Datum vraćanja</td>
      <td>Opcije</td>
  	  </b>
    </tr>
   </thead>
   <tbody>
   		@foreach($data as $z)
   		<tr>
   			<td><a href="/admin/clanovi/detalji/{{$z->id}}"> {{$z->clan->ime_prezime}} </a> </td>
   			<td> {{$z->knjiga->naslov}} </td>
   			<td  @if($z->status==1) style="background-color: rgba(250,0,0,0.5);" @else style="background-color: rgba(0,250,0,0.5);" @endif > @if( $z->status==1 ) Knjiga nije vraćena @else Knjiga je vraćena @endif </td>
        <td> {{$z->datum_zaduzenja}} </td>
        <td> {{$z->rok_vracanja}} </td>
        <td>{{$z->datum_vracanja}} </td>
        <td> <a href="/admin/zaduzenja/uredi/{{$z->id}}" class="material-icons" style="font-size: 20px;color:black"> edit </a>
              <a href="/admin/zaduzenja/del/{{$z->id}}" class="material-icons" style="font-size: 20px;color:red"> delete_forever </a>
           @if($z->status==1)   
           <a href="/admin/zaduzenja/check/{{$z->id}}" class="material-icons" style="font-size: 20px;color: blue">check</a>
           @else
           <a href="/admin/zaduzenja/check/{{$z->id}}" class="material-icons" style="font-size: 20px;color: red">cancel</a>
           @endif
        </td>
   		</tr>
   		@endforeach
   </tbody>
</table> 



@endsection