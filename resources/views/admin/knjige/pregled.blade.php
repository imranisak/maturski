@extends('master-admin')
@section('head')
<title>Pregled svih knjiga</title>
@endsection

@section('content')

<h3>Knjige</h3>
@include('partials.pik_admin')
@if($search!=1)
<p>Biblioteka ima <b>{{$broj}}</b> naslova,od toga ih je <b>{{$stanje->stanje}}</b> na stanju. </p>
@endif
@include('partials.filter_admin')
<table class="table table-hover">
  <thead>
    <tr class="table-light">
      <td scope="row">Naslov</td>
      <b>
      <td>Pisac</td>
      <td>Vrsta djela</td>
      <td>Stanje</td>
      <td>Lokacija</td>
      <td>Opcije</td>
  	  </b>
    </tr>
   </thead>
   <tbody>
   		@foreach($data as $k)
   		<tr>
   			<td><a href="/admin/knjiga/detalji/{{$k->id}}"> {{$k->naslov}} </a> </td>
   			<td> {{$k->pisac->ime_prezime}} </td>
   			<td> {{$k->vrsta->vrsta}} </td>
        <td> {{$k->količina}} </td>
        <td> {{$k->lokacija->ime}} ( {{$k->lokacija->adresa}} ) </td>
        <td> <a href="/admin/knjiga/uredi/{{$k->id}}" class="material-icons" style="font-size: 20px;color:black"> edit </a>
              <a href="/admin/knjiga/del/{{$k->id}}" class="material-icons" style="font-size: 20px;color:red"> delete_forever </a>
        </td>
   		</tr>
   		@endforeach
   </tbody>
</table> 



@endsection