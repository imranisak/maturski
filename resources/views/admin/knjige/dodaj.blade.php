@extends('master-admin')
@section('head')
<title>Dodaj knjigu</title>
@endsection

@section('content')
<h3>Dodaj knjigu</h3>
<form action="/admin/knjige/dodaj" method="post" enctype="multipart/form-data">
	{{csrf_field()}}
	<div class="form-group col-md-3">
  		<label class="col-form-label" for="inputDefault">Naslov knjige</label>
  		<input type="text" class="form-control" placeholder="Naslov" id="inputDefault" name="naslov" required>

  		<label class="col-form-label" for="inputDefault">Odaberi pisca</label>
		<select class="custom-select" required name="pisac_id">
			<option selected disabled>Odaberi pisca</option>
			@foreach($pisci as $p)
			<option value="{{$p->id}}">{{$p->ime_prezime}}</option>
			@endforeach
		</select>		

  		<label class="col-form-label" for="inputDefault">Odaberi vrstu djela</label>
		<select class="custom-select" required name="vrsta_id">
			<option selected disabled>Odaberi vrstu</option>
			@foreach($vrste as $v)
			<option value="{{$v->id}}">{{$v->vrsta}}</option>
			@endforeach
		</select>

  		<label class="col-form-label" for="inputDefault">Odaberi lokaciju</label>
		<select class="custom-select" required name="lokacija_id">
			<option selected disabled>Odaberi lokaciju</option>
			@foreach($lokacije as $l)
			<option value="{{$l->id}}">{{$l->ime}} ({{$l->adresa}}) </option>
			@endforeach
		</select>

  		<label class="col-form-label" for="inputDefault">Unesi količinu</label>
		<input type="number" name="količina" class="form-control"  placeholder="Količina" required>

  		<label class="col-form-label" for="inputDefault">Godina izdanja(optional)</label>
		<input type="number" name="godina" class="form-control"  placeholder="Godina">
		<label class="col-form-label" for="inputDefault">Naslovna slika (optional)</label>
		<input type="file" name="naslovna_slika" accept="image/*">
	</div>
	<input type="submit" value="Snimi" class="btn btn-primary">
</form>


@endsection