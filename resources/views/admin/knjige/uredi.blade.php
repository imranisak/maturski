@extends('master-admin')
@section('head')
<title>Uredi {{$data->naslov}}  </title>
@endsection
@section('content')
<form action="/admin/knjiga/uredi/{{$data->id}}" method="post">
		{{csrf_field()}}
	<div class="form-group col-md-3">
  		<label class="col-form-label" for="inputDefault">Naslov knjige</label>
  		<input type="text" class="form-control" placeholder="Naslov" id="inputDefault" name="naslov" required value="{{$data->naslov}}">

  		<label class="col-form-label" for="inputDefault">Odaberi pisca</label>
		<select class="custom-select" required name="pisac_id">
			<option  disabled>Odaberi pisca</option>
			@foreach($pisci as $p)
			<option value="{{$p->id}}"  @if($data->pisac_id == $p->id) selected @endif>{{$p->ime_prezime}}</option>
			@endforeach
		</select>		

  		<label class="col-form-label" for="inputDefault">Odaberi vrstu djela</label>
		<select class="custom-select" required name="vrsta_id">
			<option  disabled>Odaberi vrstu</option>
			@foreach($vrste as $v)
			<option value="{{$v->id}}" @if($data->vrsta->id == $v->id) selected @endif>{{$v->vrsta}}</option>
			@endforeach
		</select>
  		<label class="col-form-label" for="inputDefault">Odaberi lokaciju</label>
		<select class="custom-select" required name="lokacija_id">
			<option  disabled>Odaberi lokaciju</option>
			@foreach($lokacije as $l)
			<option value="{{$l->id}}" @if($data->lokacija->id == $l->id) selected @endif>{{$l->ime}} ({{$l->adresa}})</option>
			@endforeach
		</select>

  		<label class="col-form-label" for="inputDefault">Unesi količinu</label>
		<input type="number" name="količina" class="form-control"  placeholder="Količina" required value="{{$data->količina}}">

  		<label class="col-form-label" for="inputDefault">Godina izdanja(optional)</label>
		<input type="number" name="godina" class="form-control"  placeholder="Godina" value="{{$data->godina}}">
	</div>
	<input type="submit" value="Snimi" class="btn btn-primary">

</form>



@endsection