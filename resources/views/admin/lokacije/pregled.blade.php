@extends('master-admin')
@section('head')
<title> Pregled lokacija </title>
@endsection

@section('content')
<h3>Pregled lokacija</h3>
<table class="table table-hover">
  <thead>
    <tr class="table-light">
      <td scope="row">Ime lokacije</td>
      <td scope="row">Adresa</td>
      <td scope="row">Opcije</td>
    </tr>
   </thead>
   <tbody>
   		@foreach($data as $l)
   		<tr>
        <td> {{$l->ime}} </td>
   			<td> {{$l->adresa}} </td>
        	<td> <a href="/admin/lokacije/uredi/{{$l->id}}" class="material-icons" style="font-size: 20px;color:black"> edit </a>
              <a href="/admin/lokacije/del/{{$l->id}}" class="material-icons" style="font-size: 20px;color:red"> delete_forever </a>
        	</td>
   		</tr>
   		@endforeach
   </tbody>
</table> 

@endsection