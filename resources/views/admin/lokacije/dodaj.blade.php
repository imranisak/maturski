@extends('master-admin')
@section('head')
<title>Dodaj lokaciju</title>
@endsection

@section('content')
<h3>Dodaj lokaciju</h3>
<form action="/admin/lokacije/dodaj" method="post">
	{{csrf_field()}}
	<div class="form-group col-md-3">
  		<label class="col-form-label" for="inputDefault">Ime biblioteke / lokacije</label>
  		<input type="text" class="form-control" placeholder="Ime" id="inputDefault" name="ime" required>
	</div>	
	<div class="form-group col-md-3">
  		<label class="col-form-label" for="inputDefault">Adresa</label>
  		<input type="text" class="form-control" placeholder="Adresa" id="inputDefault" name="adresa" required>
	</div>
	<input type="submit" value="Snimi" class="btn btn-primary">
</form>


@endsection