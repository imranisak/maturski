@extends('master-admin')
@section('head')
<title>Dodaj vrstu djela</title>
@endsection

@section('content')
<h3>Dodaj lokaciju</h3>
<form action="/admin/lokacije/uredi/{{$data->id}}" method="post">
	{{csrf_field()}}
	<div class="form-group col-md-3">
  		<label class="col-form-label" for="inputDefault">Ime biblioteke / lokacije</label>
  		<input type="text" class="form-control" placeholder="Ime" id="inputDefault" name="ime" value="{{$data->ime}}" required>
	</div>	
	<div class="form-group col-md-3">
  		<label class="col-form-label" for="inputDefault">Adresa</label>
  		<input type="text" class="form-control" placeholder="Adresa" id="inputDefault" name="adresa" value="{{$data->adresa}}" required>
	</div>
	<input type="submit" value="Snimi" class="btn btn-primary">
</form>


@endsection