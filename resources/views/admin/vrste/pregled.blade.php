@extends('master-admin')
@section('head')
<title> Pregled vrsta </title>
@endsection

@section('content')

<table class="table table-hover">
  <thead>
    <tr class="table-light">
      <td scope="row">Vrsta</td>
    </tr>
   </thead>
   <tbody>
   		@foreach($data as $v)
   		<tr>
   			<td> {{$v->vrsta}} </td>
        	<td> <a href="/admin/vrste/uredi/{{$v->id}}" class="material-icons" style="font-size: 20px;color:black"> edit </a>
              <a href="/admin/vrste/del/{{$v->id}}" class="material-icons" style="font-size: 20px;color:red"> delete_forever </a>
        	</td>
   		</tr>
   		@endforeach
   </tbody>
</table> 

@endsection