@extends('master-admin')
@section('head')
<title>Uredi {{$data->vrsta}} </title>
@endsection

@section('content')
<form action="/admin/vrste/uredi/{{$data->id}}" method="post">
	{{csrf_field()}}
	<div class="form-group col-md-3">
  		<label class="col-form-label" for="inputDefault">Uredi književnu vrstu</label>
  		<input type="text" class="form-control" placeholder="Vrsta" id="inputDefault" name="vrsta" value="{{$data->vrsta}}" required>
	</div>
	<input type="submit" value="Snimi" class="btn btn-primary">
</form>

@endsection