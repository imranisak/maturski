@extends('master-admin')
@section('head')
<title>Pregled svih članova</title>

@endsection

@section('content')
@include('partials.clan_search')
<table class="table table-hover">
  <thead>
    <tr class="table-light">
      <b>
      <td scope="row">Ime i prezime</td>
      </b>
      <td>Broj telefona</td>
      <td>E-mail</td>
      <td>Opcije</td>
    </tr>
   </thead>
   <tbody>
   		@foreach($data as $c)
   		<tr>
   			<td @if($c->status==0) style="background-color: rgba(125,125,125,0.5);"  @endif ><a href="/admin/clanovi/detalji/{{$c->id}}"> {{$c->ime_prezime}} </a> </td>
   			<td> {{$c->broj}}</td>
        <td> {{$c->mail}} </td>
   			<td> 
          <a href="/admin/clanovi/uredi/{{$c->id}}" class="material-icons" style="font-size: 20px;color:black"> edit </a> 
          @if($c->status==1)
          <a href="/admin/clanovi/del/{{$c->id}}" class="material-icons" style="font-size: 20px;color:red"> delete</a>
          @else
          <a href="/admin/clanovi/del/{{$c->id}}" class="material-icons" style="font-size: 20px;color:blue"> check</a>
          @endif
        </td>
   		</tr>
   		@endforeach
   </tbody>
</table> 


@endsection