@extends('master-admin')
@section('head')
<title> Uredi {{$data->ime_prezime}} </title>
@endsection

@section('content')
<form action="/admin/clanovi/uredi/{{$data->id}}" method="post">
	{{csrf_field()}}
	<div class="form-group col-md-3">
  		<label class="col-form-label" for="inputDefault">Ime i prezime</label>
  		<input type="text" class="form-control" placeholder="Ime i prezime" id="inputDefault" name="ime_prezime" value=" {{$data->ime_prezime}} " required>  		
  		<label class="col-form-label" for="inputDefault" >E-Mail </label>
  		<input type="mail" class="form-control" placeholder="E-mail" id="inputDefault" name="nacionalnost" required value=" {{$data->mail}} " >
  		<label class="col-form-label" for="inputDefault">Broj telefona</label>
  		<input type="text" class="form-control" placeholder="Broj Telefona" id="inputDefault" name="broj" required value="{{$data->broj}}">
	</div>
	<input type="submit" value="Uredi" class="btn btn-primary">
</form>
@endsection