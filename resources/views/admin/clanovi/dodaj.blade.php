@extends('master-admin')
@section('head')
<title>Dodaj člana biblioteke</title>
@endsection

@section('content')
<h3>Dodaj člana biblioteke</h3>
<form action="/admin/clanovi/dodaj" method="post">
	{{csrf_field()}}
	<div class="form-group col-md-3">
  		<label class="col-form-label" for="inputDefault">Ime i prezime</label>
  		<input type="text" class="form-control" placeholder="Ime i prezime" id="inputDefault" name="ime_prezime" required>  		
  		<label class="col-form-label" for="inputDefault">E-mail</label>
  		<input type="mail" class="form-control" placeholder="E-mail" id="inputDefault" name="mail" required>
  		<label class="col-form-label" for="inputDefault">Broj telefona</label>
      <input type="text" class="form-control" placeholder="Telefon" id="inputDefault" name="broj" required>         
  		<input type="hidden" class="form-control"  id="inputDefault" name="status" value="1" required>  				
	</div>
	<input type="submit" value="Snimi" class="btn btn-primary">
</form>


@endsection