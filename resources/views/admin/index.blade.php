@extends('master-admin')
@section('head')
<title>Admin home</title>
@endsection

@section('content')
@include('partials.charts')
<h1>Dobrodošli!</h1>
	<span>
		<div id="piechart"></div>
	<div class="progress">
		

  <div class="progress-bar" role="progressbar" style="width:{{$proc}}%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">{{$proc}}% knjiga na stanju. ( {{$stanjebr->stanje}} ) </div>
	</span>
@endsection