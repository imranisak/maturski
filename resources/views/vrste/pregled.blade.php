@extends('master')
@section('head')
<title> Pregled vrsta </title>
@endsection

@section('content')

<table class="table table-hover">
  <thead>
    <tr class="table-light">
      <td scope="row">Vrsta</td>
    </tr>
   </thead>
   <tbody>
   		@foreach($data as $v)
   		<tr>
   			<td> {{$v->vrsta}} </td>
   		</tr>
   		@endforeach
   </tbody>
</table> 

@endsection