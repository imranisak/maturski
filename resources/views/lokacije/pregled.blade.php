@extends('master')
@section('head')
<title> Pregled lokacija </title>
@endsection

@section('content')
<h3>Pregled lokacija</h3>
<table class="table table-hover">
  <thead>
    <tr class="table-light">
      <td scope="row">Ime lokacije</td>
      <td scope="row">Adresa</td>
    </tr>
   </thead>
   <tbody>
   		@foreach($data as $l)
   		<tr>
        <td> {{$l->ime}} </td>
   			<td> {{$l->adresa}} </td>
   		</tr>
   		@endforeach
   </tbody>
</table> 

@endsection