<!DOCTYPE html>
<html>
<head>
	@yield('head')
	@include('partials.head-assets')
</head>
<body>
	<div class="container">
		<div class="col-md-13">
			@include('partials.nav-admin')
			@include('partials.alerts')
			@yield('content')
		</div>
		
	</div>
</body>
</html>