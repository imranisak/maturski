@extends('master')
@section('head')
<title>Pregled svih pisaca</title>

@endsection

@section('content')
@include('partials.pik')
<table class="table table-hover">
  <thead>
    <tr class="table-light">
      <b>
      <td scope="row">Ime i prezime</td>
      </b>
      <td>Godina rođenja i smrti</td>
      <td>Nacionalnost</td>
  	  </b>
    </tr>
   </thead>
   <tbody>
   		@foreach($data as $p)
   		<tr>
   			<td><a href="/pisci/detalji/{{$p->id}}"> {{$p->ime_prezime}} </a> </td>
   			<td> {{$p->rodjenje}} @if($p->smrt!=0) - {{$p->smrt}} @endif  </td>
        <td> {{$p->nacionalnost}} </td>
   		</tr>
   		@endforeach
   </tbody>
</table> 


@endsection