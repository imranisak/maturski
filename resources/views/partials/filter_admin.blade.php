<form method="get" action="/admin/knjiga/filter">
	{{csrf_field()}}
	<div class="form-row">
		<div class="col-lg-2">
	    	<select class="custom-select " name="pisac_filter">
	      		<option  selected value="0">Pisac...</option>
				@foreach($pisci as $p)
				<option value=" {{$p->id}} "  @if($filter==1) @if($p->id==$pisacfilter) selected @endif @endif  > {{$p->ime_prezime}} </option>
				@endforeach
	    	</select>    	
		</div>

		<div class="col-lg-2">
	    	<select class="custom-select" name="vrsta_filter">
	      		<option  selected value="0">Vrsta...</option>
				@foreach($vrsta as $v)
				<option value=" {{$v->id}} " @if($filter==1) @if($v->id==$vrstafilter) selected @endif @endif> {{$v->vrsta}} </option>
				@endforeach
	    	</select>
	    </div>

	    <div class="col-lg-2">
	    	<select class="custom-select" name="lokacija_filter">
	      		<option  selected value="0">Lokacija...</option>
				@foreach($lokacije as $l)
				<option value=" {{$l->id}} " @if($filter==1) @if($l->id==$lokacijafilter) selected @endif @endif> {{$l->ime}} ({{$l->adresa}}) </option>
				@endforeach
	    	</select>
	    </div>	    

	    <div class="col-lg-2">
	    	<select class="custom-select" name="prikaz">
	      		<option disabled>Prikaži...</option>
				<option value="10" @if($filter==1) @if($prikaz==10) selected @endif @endif> 10 </option>
				<option value="20" @if($filter==1) @if($prikaz==20) selected @endif @endif> 20 </option>
				<option value="50" @if($filter==1) @if($prikaz==50) selected @endif @endif> 50 </option>
				<option value="0" @if($filter==1) @if($prikaz==0) selected @endif @endif> Sve </option>
	    	</select>
	    </div>
	    <input type="submit" class="btn btn-primary" value="Filtriraj">
	    <a href="/admin/knjige/index" class="btn btn-danger">Resetuj</a>
  </div>	

</form>