@if(Session::has('success')) 
	<div class="alert alert-success alert-dismissable">
	  	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	  	<strong></strong> {{Session::get('success')}}
	</div>
@endif
@if(Session::has('error')) 
	<div class="alert alert-danger alert-dismissable">
	  	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	  	<strong>{{Session::get('error')}}</strong>
	</div>
@endif