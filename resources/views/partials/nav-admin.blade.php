<nav class="navbar navbar-expand-lg navbar-dark bg-danger">
  <a class="navbar-brand" href="/admin">Maturski</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav mr-auto">
  <!--Nav za knjige -->
  <li class="nav-item dropdown show">
    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="true">Knjige</a>
    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;">
      <a class="dropdown-item" href="/admin/knjige/dodaj">Dodaj knjigu</a>
      <a class="dropdown-item" href="/admin/knjige/index">Pregled knjiga</a>
    </div>
  </li>
  <!--Nav za pisce -->
  <li class="nav-item dropdown show">
    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="true">Pisci</a>
    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;">
      <a class="dropdown-item" href="/admin/pisci/dodaj">Dodaj pisca</a>
      <a class="dropdown-item" href="/admin/pisci/index">Pregled pisaca</a>
    </div>
  </li>
  <!--nav za vrste -->
  <li class="nav-item dropdown show">
    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="true">Književne vrste</a>
    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;">
      <a class="dropdown-item" href="/admin/vrste/dodaj">Dodaj književnu vrstu</a>
      <a class="dropdown-item" href="/admin/vrste/index">Pregled književnih vrsta</a>
    </div>
  </li>   
  <!-- nav za lokacije -->
  <li class="nav-item dropdown show">
    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="true">Lokacije</a>
    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;">
      <a class="dropdown-item" href="/admin/lokacije/dodaj">Dodaj lokaciju</a>
      <a class="dropdown-item" href="/admin/lokacije/index">Pregled lokacije</a>
    </div>
  </li>    
  <!-- nav za članove -->
  <li class="nav-item dropdown show">
    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="true">Članovi</a>
    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;">
      <a class="dropdown-item" href="/admin/clanovi/dodaj">Dodaj člana</a>
      <a class="dropdown-item" href="/admin/clanovi/index">Pregled članova</a>
    </div>
  </li>  
  <!-- nav za zadužanja -->
  <li class="nav-item dropdown show">
    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="true">Zaduženja</a>
    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;">
      <a class="dropdown-item" href="/admin/zaduzenja/dodaj">Dodaj zaduženje</a>
      <a class="dropdown-item" href="/admin/zaduzenja/index">Pregled zaduženja</a>
    </div>
  </li>     
      <li class="nav-item">
        <a href="/logout" class="btn btn-danger">Logout</a>
      </li>
    </ul>
  </div>
</nav>