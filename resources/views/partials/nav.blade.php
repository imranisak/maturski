<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="/">Maturski</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="/knjige">Knjige</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/pisci">Pisci</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/vrste">Vrste djela</a>
      </li>       
      <li class="nav-item">
        <a class="nav-link" href="/lokacije">Lokacije</a>
      </li>      
      <li class="nav-item">
        <a href="/login" class="btn btn-primary">Login</a>
      </li> 
    </ul>
  </div>
</nav>