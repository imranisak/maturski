<?php

//Rute koje zahtjevaju login, admin only.
Route::group(['middleware'=>['auth']], function(){
Route::get('/admin','index@admin');
//Tražilica
Route::get('/admin/pik', 'pik@pikadmin');
Route::get('/admin/clansearch', 'clan_search@search');


//Vrste
Route::get('/admin/vrste/index', 'VrsteController@index');
Route::get('/admin/vrste/dodaj', 'VrsteController@create');
Route::post('/admin/vrste/dodaj', 'VrsteController@store');
Route::get('/admin/vrste/uredi/{id}', 'VrsteController@edit');
Route::post('/admin/vrste/uredi/{id}', 'VrsteController@update');
Route::get('/admin/vrste/del/{id}', 'VrsteController@destroy');

//Pisci
Route::get('/admin/pisci/dodaj', 'PisciController@create');
Route::post('/admin/pisci/dodaj', 'PisciController@store');
Route::get('/admin/pisci/index','PisciController@index');
Route::get('/admin/pisci/detalji/{id}','PisciController@show');
Route::get('/admin/pisci/uredi/{id}','PisciController@edit');
Route::post('/admin/pisci/uredi/{id}','PisciController@update');
Route::get('/admin/pisci/del/{id}','PisciController@destroy');

//Knjige
Route::get('/admin/knjige/dodaj', 'KnjigeController@create');
Route::post('/admin/knjige/dodaj', 'KnjigeController@store');
Route::get('/admin/knjige/index', 'KnjigeController@index');
Route::get('/admin/knjiga/detalji/{id}' , 'KnjigeController@show');
Route::get('/admin/knjiga/uredi/{id}' , 'KnjigeController@edit');
Route::post('/admin/knjiga/uredi/{id}' , 'KnjigeController@update');
Route::get('/admin/knjiga/del/{id}' , 'KnjigeController@destroy');
Route::get('/admin/knjiga/filter' , 'KnjigeController@filteradmin');

//Lokacije
Route::get('/admin/lokacije/dodaj', 'LokacijeController@create');
Route::post('/admin/lokacije/dodaj', 'LokacijeController@store');
Route::get('/admin/lokacije/index', 'LokacijeController@index');
Route::get('/admin/lokacije/detalji/{id}' , 'LokacijeController@show');
Route::get('/admin/lokacije/uredi/{id}' , 'LokacijeController@edit');
Route::post('/admin/lokacije/uredi/{id}' , 'LokacijeController@update');
Route::get('/admin/lokacije/del/{id}' , 'LokacijeController@destroy');

//Clanovi
Route::get('/admin/clanovi/dodaj', 'ClanController@create');
Route::post('/admin/clanovi/dodaj', 'ClanController@store');
Route::get('/admin/clanovi/index', 'ClanController@index');
Route::get('/admin/clanovi/detalji/{id}' , 'ClanController@show');
Route::get('/admin/clanovi/uredi/{id}' , 'ClanController@edit');
Route::post('/admin/clanovi/uredi/{id}' , 'ClanController@update');
Route::get('/admin/clanovi/del/{id}' , 'ClanController@destroy');

//Zaduzenja
Route::get('/admin/zaduzenja/dodaj', 'ZaduzenjaController@create');
Route::post('/admin/zaduzenja/dodaj', 'ZaduzenjaController@store');
Route::get('/admin/zaduzenja/index', 'ZaduzenjaController@index');
Route::get('/admin/zaduzenja/detalji/{id}' , 'ZaduzenjaController@show');
Route::get('/admin/zaduzenja/uredi/{id}' , 'ZaduzenjaController@edit');
Route::post('/admin/zaduzenja/uredi/{id}' , 'ZaduzenjaController@update');
Route::get('/admin/zaduzenja/del/{id}' , 'ZaduzenjaController@destroy');
Route::get('/admin/zaduzenja/check/{id}' , 'ZaduzenjaController@check');
/////////////////////////////////////////////////////////////////////////
});
//Rute za korisnika
Route::get('/', 'index@index');
Route::get('/vrste', 'VrsteController@indexuser');
Route::get('/knjige', 'KnjigeController@indexuser');
Route::get('/knjiga/detalji/{id}', 'KnjigeController@showuser');
Route::get('/knjige/filter', 'KnjigeController@filter');
Route::get('/pisci', 'PisciController@indexuser');
Route::get('/pisci/detalji/{id}', 'PisciController@showuser');
Route::get('/lokacije', 'LokacijeController@indexuser');
Route::get('/pik', 'pik@pik');


//LOGIN i LOGOUT
Route::get('/login','Auth\LoginController@showLoginForm')->name('login');
Route::post('/login','Auth\LoginController@login');
Route::get('/logout','Auth\LoginController@logout');

Route::get('/passwordrequest','Auth\ResetPassowrd@showLoginForm');