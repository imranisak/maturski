<?php

namespace App\Http\Controllers;
use App\Knjige;
use App\Pisci;
use App\Lokacije;
use App\Vrste;

use Illuminate\Http\Request;

class pik extends Controller
{
    public function pik(Request $Request)
    {
    	if($Request->vrsta==1)
    	{
    		$filter=0;
    		$pisci=Pisci::all();
        	$lokacije=Lokacije::all();
        	$vrsta=Vrste::all();
    		$search=1;
    		$data=Knjige::where('naslov',$Request->search)->get();
			return view('knjige.pregled',compact('data','search','pisci','lokacije','vrsta','filter'));
    	}
    	else
    	{
    		$data=Pisci::where('ime_prezime',$Request->search)->get();
    		return view('pisci.pregled',compact('data'));
    	}
    }    

    public function pikadmin(Request $Request)
    {
    	if($Request->vrsta==1)
    	{
    		$filter=0;
    		$pisci=Pisci::all();
        	$lokacije=Lokacije::all();
        	$vrsta=Vrste::all();
    		$search=1;
    		$data=Knjige::where('naslov',$Request->search)->get();
			return view('admin.knjige.pregled',compact('data','search','pisci','vrsta','lokacije','filter'));
    	}
    	else
    	{
    		$data=Pisci::where('ime_prezime',$Request->search)->get();
    		return view('admin.pisci.pregled',compact('data'));
    	}
    }
}
