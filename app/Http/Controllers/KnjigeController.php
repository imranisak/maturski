<?php

namespace App\Http\Controllers;
use DB;
use App\Knjige;
use App\Pisci;
use App\Vrste;
use App\Lokacije;
use App\Zaduzenja;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class KnjigeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filter=0;
        $data=Knjige::where('id','>',1);
        $pisci=Pisci::all();
        $lokacije=Lokacije::all();
        $vrsta=Vrste::all();        
        ///dd($data);
        $stanje=DB::table('knjiges')->select(DB::raw('sum(količina) as stanje'))->first();
        //dd($stanje);
        $search=0;
        $data=$data->orderBy('naslov','asc')->get();
        $broj=count($data);
        return view ('admin.knjige.pregled',compact('data','broj','stanje','search','pisci','lokacije','filter','vrsta'));
    }
    public function indexuser()
    {
        $filter=0;
        $data=Knjige::where('id','>',1);
        $pisci=Pisci::all();
        $lokacije=Lokacije::all();
        $vrsta=Vrste::all();
        ///dd($data);
        $stanje=DB::table('knjiges')->select(DB::raw('sum(količina) as stanje'))->first();
        //dd($stanje);
        $data=$data->orderby('naslov','asc')->get();
        $broj=count($data);
        $search=0;
        return view ('knjige.pregled',compact('data','broj','stanje','search','lokacije','pisci','vrsta','filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $pisci=Pisci::all();
        //dd($pisci);
        $vrste=Vrste::all();
        $lokacije=Lokacije::all();
        return view('admin.knjige.dodaj',compact('pisci','vrste','lokacije'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Knjige::create($request->all());
       // dd($request);
        if ($request->hasfile('naslovna_slika'))
        {
            $imeEXT=$request->file('naslovna_slika')->getClientOriginalName();
            $ime=pathinfo($imeEXT, PATHINFO_FILENAME);
            $EXT=$request->file('naslovna_slika')->getClientOriginalExtension();
            $snimi=$ime.'_'.time().'.'.$EXT;
            Storage::put('naslovna_slika', $snimi);
        }
        else $snimi='slikaND.jpg';
        $knjiga= new Knjige;
        $knjiga->naslov=$request->input('naslov');
        $knjiga->pisac_id=$request->input('pisac_id');
        $knjiga->količina=$request->input('količina');
        $knjiga->godina=$request->input('godina');
        $knjiga->vrsta_id=$request->input('vrsta_id');
        $knjiga->lokacija_id=$request->input('lokacija_id');
        $knjiga->naslovna_slika=$snimi;
        $knjiga->save();
        session()->flash('success','Knjiga uspješno snimljena');
        return redirect('/admin/knjige/dodaj');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Knjige  $knjige
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Knjige::find($id);
        return view('admin.knjige.detalji',compact('data'));
    }    

    public function showuser($id)
    {
        $data=Knjige::find($id);
        return view('knjige.detalji',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Knjige  $knjige
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pisci=Pisci::all();
        $vrste=Vrste::all();
        $lokacije=Lokacije::all();
        $data=Knjige::find($id);
        //dd($data);
        return view ('admin.knjige.uredi',compact('data','pisci','vrste','lokacije'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Knjige  $knjige
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $knjiga=Knjige::find($id);
        $knjiga->update($request->all());
        $data=Knjige::find($id);
        $vrste=Vrste::all();
        $pisci=Pisci::all();
        $lokacije=Lokacije::all();
        session()->flash('success','Knjiga uspješno uređena');
        return view('admin.knjige.uredi',compact('data', 'vrste', 'pisci','lokacije'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Knjige  $knjige
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Zaduzenja::where('knjiga_id',$id)->get();
        if (count($data)!=0) 
        {
            session()->flash('error','Odabrana knjiga je povezana za neko zaduženje, te je nije moguće obrisati.');
            return redirect('/admin/knjige/index');
        }
        else
        {
        Knjige::destroy($id);
        session()->flash('success','Knjige uspješno obrisana.');
        return redirect('/admin/knjige/index');

        }
    }
    public function filter(Request $Request)
    {
        $filter=1;
        $prikaz=$Request->prikaz;
        $pisacfilter=$Request->pisac_filter;
        $vrstafilter=$Request->vrsta_filter;
        $lokacijafilter=$Request->lokacija_filter;
        $data=Knjige::where('id','>',0);
        $pisci=Pisci::all();
        $lokacije=Lokacije::all();
        $vrsta=Vrste::all();
        if(($Request->pisac_filter)!=0) $data->where('pisac_id',$Request->pisac_filter);
        if(($Request->vrsta_filter)!=0) $data->where('vrsta_id',$Request->vrsta_filter);
        if(($Request->lokacija_filter)!=0) $data->where('lokacija_id',$Request->lokacija_filter);
        if(($Request->prikaz)!=0) $data=$data->orderBy('naslov','asc')->take(($Request->prikaz))->get();
        else $data=$data->orderBy('naslov','asc')->get();
        //dd($data);
        $search=1;
        return view('knjige.pregled',compact('data','search','pisci','lokacije','vrsta','filter','pisacfilter','vrstafilter','lokacijafilter','prikaz'));
    }    

    public function filteradmin(Request $Request)
    {
        $prikaz=$Request->prikaz;
        $pisacfilter=$Request->pisac_filter;
        $vrstafilter=$Request->vrsta_filter;
        $lokacijafilter=$Request->lokacija_filter;
        $data=Knjige::where('id','>',0);
        $pisci=Pisci::all();
        $lokacije=Lokacije::all();
        $vrsta=Vrste::all();
        if(($Request->pisac_filter)!=0) $data->where('pisac_id',$Request->pisac_filter);
        if(($Request->vrsta_filter)!=0) $data->where('vrsta_id',$Request->vrsta_filter);
        if(($Request->lokacija_filter)!=0) $data->where('lokacija_id',$Request->lokacija_filter);
        if(($Request->prikaz)!=0) $data=$data->orderBy('naslov','asc')->take(($Request->prikaz))->get();
        else $data=$data->orderBy('naslov','asc')->get();
        $filter=1;
        //dd($data);
        $search=1;
        return view('admin.knjige.pregled',compact('data','search','pisci','lokacije','vrsta','filter','pisacfilter','vrstafilter','lokacijafilter','prikaz'));
    }
}
