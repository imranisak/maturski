<?php

namespace App\Http\Controllers;

use App\Lokacije;
use App\knjige;
use Illuminate\Http\Request;

class LokacijeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Lokacije::all();
        return view('admin.lokacije.pregled',compact('data'));
    }    

    public function indexuser()
    {
        $data=Lokacije::all();
        return view('lokacije.pregled',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.lokacije.dodaj');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Lokacije::create($request->all());
        session()->flash('success','Lokacije uspješno snimljena.');
        return redirect('/admin/lokacije/dodaj');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lokacije  $lokacije
     * @return \Illuminate\Http\Response
     */
    public function show(Lokacije $lokacije)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lokacije  $lokacije
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Lokacije::find($id);
        return view('admin.lokacije.uredi',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lokacije  $lokacije
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=Lokacije::find($id);
        $data->update($request->all());
        session()->flash('success','Lokacija uspješnu uređena');
        return view('admin.lokacije.uredi',compact('data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lokacije  $lokacije
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Knjige::where('lokacija_id',$id)->get();
        if (count($data)>0) {
            session()->flash('error','Lokacija je već povezana za neku Knjigu, tako da je nije moguće obrisati.');
            return redirect('/admin/lokacije/index');
        }
        else{
            Lokacije::destroy($id);
            session()->flash('success','Lokacija obrisana.');
            return redirect('/admin/lokacije/index');
        }
    }
}
