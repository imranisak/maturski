<?php

namespace App\Http\Controllers;
use App\Clan;
use Illuminate\Http\Request;

class clan_search extends Controller
{
	public function search(Request $Request)
	{
		$data=Clan::where('ime_prezime',$Request->search)->get();
    	return view('admin.clanovi.pregled',compact('data'));
	}

}
