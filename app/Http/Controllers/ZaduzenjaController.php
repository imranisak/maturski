<?php

namespace App\Http\Controllers;

use App\Zaduzenja;
use App\Clan;
use App\Knjige;
use DB;
use Illuminate\Http\Request;

class ZaduzenjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Zaduzenja::where('id','>',0);
        $data=$data->orderBy('status','desc')->get();
       // dd($data);   
        return view('admin.zaduzenja.pregled',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clan=Clan::all();
        $knjige=Knjige::all();
        return view('admin.zaduzenja.dodaj',compact('clan','knjige'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->get('knjiga_id'));
        if($request->get('knjiga_id')==0 || $request->get('clan_id')==0)
        {
            session()->flash('error','Morate odabrati knjigu/člana.');
            return redirect('/admin/zaduzenja/dodaj');
        }
        else
        {
            $clan=Zaduzenja::where('clan_id',$request->get('clan_id'));
            $clan=$clan->where('status',1)->get();
            //dd($clan);   
            if (count($clan)>=2){
              session()->flash('error','Izabrani čitaoc je već podigao 2 knjige koje nije vratio, tako da ne može podići još jednu knjigu.');
              return redirect('/admin/zaduzenja/dodaj');
            } 
            //dd($knjiga);
            $knjiga=Knjige::find($request->get('knjiga_id'));
            $minus=$knjiga->količina-1;
            $knjiga->količina=$minus;
            $knjiga->save();
            Zaduzenja::create($request->all());
            session()->flash('success','Zaduženje dodano');
            return redirect('/admin/zaduzenja/dodaj');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Zaduzenja  $zaduzenja
     * @return \Illuminate\Http\Response
     */
    public function show(Zaduzenja $zaduzenja)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Zaduzenja  $zaduzenja
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Zaduzenja::find($id);
        $clan=Clan::all();
        $knjige=Knjige::all();
        return view('admin.zaduzenja.uredi',compact('data','clan','knjige'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Zaduzenja  $zaduzenja
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        //Provjerava da li je odabrani član već zadužen sa dvije knjige
       /* $clan=Zaduzenja::where('clan_id',$request->get('clan_id'));
        $clan=$clan->where('status',1)->get();
        //dd($clan);   
        if (count($clan)>=2){
        session()->flash('error','Izabrani čitaoc je već podigao 2 knjige koje nije vratio, tako da ne može podići još jednu knjigu.');
        return $this->edit($id);
        } */

        $data=Zaduzenja::find($id);
        $stara_knjiga=$data->knjiga_id;
        //dd($stara_knjiga);
        $nova_knjiga=$request->get('knjiga_id');
        //Provjeri da li je knjiga zaduženja mijenjanja.
        if ($stara_knjiga==$nova_knjiga) 
        {
            $data->update($request->all());
            session()->flash('success','Zaduzenje uređeno.');
            return $this->edit($id);
        }
        else
        {
           //Vraća staru knjigu i uzima novu 
           $stara_k=Knjige::find($stara_knjiga);
           //dd($stara_k);
           $kolicina=$stara_k->količina+1;
           $stara_k->količina=$kolicina;
           $stara_k->save();
           
           $nova_k=Knjige::find($request->get('knjiga_id'));
           $nova_kol=$nova_k->količina-1;
           $nova_k->količina=$nova_kol;
           $nova_k->save();

            $data->update($request->all());
            session()->flash('success','Zaduzenje uređeno, stara knjiga vraćena u bazu i nova oduzeta.');
            return $this->edit($id);

        }

        //return redirect('/admin/zaduzenja/uredi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Zaduzenja  $zaduzenja
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $zaduzenje=Zaduzenja::find($id);
        $status=$zaduzenje->status;
        if ($status==0) {
            Zaduzenja::destroy($id);
            session()->flash('success','Zaduženje obrisano.');
            return redirect('/admin/zaduzenja/index');
        }
        else{
            $knjiga_id=$zaduzenje->knjiga_id;
            $knjiga=Knjige::find($knjiga_id);
            $kolicina=$knjiga->količina+1;
            $knjiga->količina=$kolicina;
            $knjiga->save();
            Zaduzenja::destroy($id);
            session()->flash('success','Zaduženje obirsano te je knjiga vraćena u bazu.');
            return redirect('/admin/zaduzenja/index');
        }
    }
    public function check($id)
    {
        $zaduzenje=Zaduzenja::find($id);
        $knjiga_id=$zaduzenje->knjiga_id;
        $status=$zaduzenje->status;
        if($status==1){
            //Postavi datum vraćanja i status
            $zaduzenje->status=0;
            $zaduzenje->datum_vracanja=date('Y-m-d');
            $zaduzenje->save();
            //////////////////////////////////
            //Vrati knjigu u bazu, tj. poveća za 1
            $knjiga=Knjige::find($knjiga_id);
            $kolicina=$knjiga->količina+1;
            $knjiga->količina=$kolicina;
            $knjiga->save();
            session()->flash('success','Član razdužen i knjiga je vraćena u bazu.');
            return redirect('/admin/zaduzenja/index');
            //////////////////////////////////////
        }
        else {
            //Za poništavanje zaduženja, prvo provjerava da li ima dovoljno knjiga
            $knjiga=Knjige::find($knjiga_id);
            if($knjiga->količina<=0){
                session()->flash('error','Radnju je nemoguće uraditi jer nema dovoljno knjiga u bazi.');
                return redirect('/admin/zaduzenja/index');
            }
            /////////////////////////////////////////////////////////////////////
            //Vrati zaduženje
            else{   
            $zaduzenje->status=1;
            $zaduzenje->datum_vracanja=null;
            $zaduzenje->save();
            $knjiga_id=$zaduzenje->knjiga_id;
            $kolicina=$knjiga->količina-1;
            $knjiga->količina=$kolicina;
            $knjiga->save();
            session()->flash('success','Zaduženje je vraćeno i knjiga je oduzeta iz baze.');
            return redirect('/admin/zaduzenja/index');
            }
            //////////////
        }
    }
}
