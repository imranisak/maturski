<?php

namespace App\Http\Controllers;

use App\Pisci;
use App\Knjige;
use DB;
use Illuminate\Http\Request;

class PisciController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Pisci::all();
        $broj=count($data);
        return view('admin.pisci.pregled',compact('data','broj'));
    }    

    public function indexuser()
    {
        $data=Pisci::all();
        $broj=count($data);
        return view('pisci.pregled',compact('data','broj'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pisci=Pisci::all();
        return view('admin.pisci.dodaj',compact('pisci'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Pisci::create($request->all());
        session()->flash('success','Pisac uspješno dodan.');
        return redirect('/admin/pisci/dodaj');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pisci  $pisci
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Pisci::find($id);
        $knjige=Knjige::where('id','>',0); 
        $knjige->where('pisac_id',$id);
        $knjige=$knjige->orderBy('id','desc')->get();
        //dd($knjige);
        return view('admin.pisci.detalji',compact('data','knjige'));
    }    

    public function showuser($id)
    {
        $data=Pisci::find($id);
        $knjige=Knjige::where('id','>',0); 
        $knjige->where('pisac_id',$id);
        $knjige=$knjige->orderBy('id','desc')->get();
        //dd($knjige);
        return view('pisci.detalji',compact('data','knjige'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pisci  $pisci
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Pisci::find($id);
        return view('admin.pisci.uredi',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pisci  $pisci
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=Pisci::find($id);
        $data->update($request->all());
        session()->flash('success','Pisac uspješno uređen.');
        return view('admin.pisci.uredi',compact('data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pisci  $pisci
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $knjige=Knjige::where('pisac_id',$id)->get();
        if (count($knjige)>0) {
            session()->flash('error','Ovaj pisac je već povezan za neku knjigu, te ga nije moguće obrisati.');
            return redirect('/admin/pisci/index');
        }
        else{
            Pisci::destroy($id);
            session()->flash('success','Pisac obrisan.');
            return redirect('/admin/pisci/index');
        }
    }
}
