<?php

namespace App\Http\Controllers;

use App\Clan;
use App\Zaduzenja;
use Illuminate\Http\Request;

class ClanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Clan::where('id','>',0);
        $data=$data->orderBy('status','desc');
        $data=$data->orderBy('ime_prezime','asc')->get();
        return view('admin.clanovi.pregled',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.clanovi.dodaj');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Clan::create($request->all());
        session()->flash('success','Član uspješno dodan.');
        return redirect('/admin/clanovi/dodaj');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Clan  $clan
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Clan::find($id);
        return view('admin.clanovi.detalji',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Clan  $clan
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Clan::find($id);
        return view('admin.clanovi.uredi',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Clan  $clan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=Clan::find($id);
        $data->update($request->all());
        session()->flash('success','Član uspješno uređen.');
        return view('admin.clanovi.uredi',compact('data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Clan  $clan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $broj=Zaduzenja::where('clan_id',$id)->get();
        if (count($broj)!=0) 
        {
            session()->flash('error','Član je zadužen te ga nije moguće obrisati.');
            return redirect('/admin/clanovi/index');
        }
        else
        {
            $data=Clan::find($id);
            $status=$data->status;
            if ($status==1)
            {
                $data->status=0;
                $data->save();
                session()->flash('success','Status člana postaveljen na "neaktivan".');
                return redirect('/admin/clanovi/index');
            }
            else
            {
                $data->status=1;
                $data->save();
                session()->flash('success','Status člana postaveljen na "aktivan".');
                return redirect('/admin/clanovi/index');
            }
        }

    }
}
