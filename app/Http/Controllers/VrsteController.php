<?php

namespace App\Http\Controllers;

use App\Vrste;
use App\Knjige;
use Illuminate\Http\Request;

class VrsteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Vrste::all();
        return view ('admin.vrste.pregled',compact('data'));
    }    

    public function indexuser()
    {
        $data=Vrste::all();
        return view ('vrste.pregled',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=Vrste::all();
        //dd($data);
        return view('admin.vrste.dodaj',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Vrste::create($request->all());
        session()->flash('success','Književna vrsta dodana');
        return redirect('/admin/vrste/dodaj');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vrste  $vrste
     * @return \Illuminate\Http\Response
     */
    public function show(Vrste $vrste)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vrste  $vrste
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Vrste::find($id);
        return view('admin.vrste.uredi',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vrste  $vrste
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=Vrste::find($id);
        $data->update($request->all());
        session()->flash('success','Vrsta uređena.');
        return view('admin.vrste.uredi',compact('data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vrste  $vrste
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Knjige::where('vrsta_id',$id)->get();
        //dd($data);
        if (count($data)>0) {
            session()->flash('error','Ova vrsta je već povezana za neku knjigu, te ne može biti obrisana.');
            return redirect('/admin/vrste/index');
        }
        else{
            Vrste::destroy($id);
            session()->flash('success','Vrsta uspješno obrisana.');
            return redirect('/admin/vrste/index/');
        }
    }
}
