<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Zaduzenja;
use App\Clan;
use App\Lokacije;
use App\Knjige;

class index extends Controller
{
    public function index()
    {
    	$knjige=count(Knjige::all());
    	$lokacije=count(Lokacije::all());
        $stanje=DB::table('knjiges')->select(DB::raw('sum(količina) as stanje'))->first();
       // dd($stanje);

    	return view('home',compact('knjige','lokacije','stanje'));
    }
    public function admin()
    {
        //Broj članova
        $svicl=count(Clan::all());
        $aktivnicl=Clan::where('status','>',0)->get();
        $aktivnicl=count($aktivnicl);
        //Procenat knjiga
        $stanje=count(Knjige::all());
        $nema=Knjige::where('količina',0)->get();
        $nema=count($nema);
        $proc=($stanje - $nema)/$stanje*100;
        $proc=(int)$proc;
        //dd($proc);
        $stanjebr=DB::table('knjiges')->select(DB::raw('sum(količina) as stanje'))->first();

        return view('admin.index',compact('svicl','aktivnicl','proc','stanjebr'));
    }
}
