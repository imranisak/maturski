<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zaduzenja extends Model
{
    protected $fillable = ['clan_id','knjiga_id','status','datum_zaduzenja','datum_vracanja','rok_vracanja'];

    public function clan(){
    	return $this->belongsTo(Clan::class);
    }    
    public function knjiga(){
    	return $this->belongsTo(Knjige::class);
    }
} 
