<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lokacije extends Model
{
    protected $fillable=['ime','adresa'];
}
