<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Knjige extends Model
{
    protected $fillable =['naslov','pisac_id','količina', 'godina', 'vrsta_id','lokacija_id'];

    public function pisac(){
    return $this->belongsTo(Pisci::class);
    }    
    public function vrsta(){
    return $this->belongsTo(Vrste::class);
    }    
    public function lokacija(){
    return $this->belongsTo(Lokacije::class);
    }
}
