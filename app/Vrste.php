<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vrste extends Model
{
    protected $fillable =['vrsta'];
}
