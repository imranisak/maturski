<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pisci extends Model
{
    protected $fillable = ['ime_prezime','rodjenje','smrt','nacionalnost'];
}
